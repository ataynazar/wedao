import React from 'react';
import './MarginedWrapper.scss'

const PerformedArtistsComponent = ({children,flex}) => (
        <div className="margined-wrapper" style={flex === true ? {display: 'flex'} : null }>
            {children}
        </div>
    );

export default PerformedArtistsComponent