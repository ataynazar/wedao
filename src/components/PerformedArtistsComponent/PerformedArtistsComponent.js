import React from 'react';
import './PerformedArtistsComponent.scss'
import artist1 from './artist-1.png'
import artist2 from './artist-2.png'
import artist3 from './artist-3.png'
import artist4 from './artist-4.png'
import artist5 from './artist-5.png'

const artists = [
    {name: 'Janet Jackson', image: artist1},
    {name: 'Gypsy Kings', image: artist2},
    {name: 'Depeche Mode', image: artist3},
    {name: 'Tom Petty', image: artist4},
    {name: 'Tiesto', image: artist5}
]

const PerformedArtistsComponent = () => (
        <div className="performed-artists-component-container">
            <div className="title">Performed at this venue</div>
            <div className="artists-container">
                {
                    artists.map(artist => {
                        return (
                            <div className="artist-container">
                                <div className="image" style={{ backgroundImage: `url(${artist.image})` }}></div>
                                <div className="name">{artist.name}</div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
);

export default PerformedArtistsComponent;