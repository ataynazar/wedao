import React, { Component } from "react";
import "./LandingSearch.scss";

class LandingSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      names: ["Bob Dylan", "Bob Marley", "Bob Moses"],
      filteredNames: []
    };
  }

  handleSearch = e => {
    const filtered = this.state.names.filter(name => {
      if (e.target.value && name.toLowerCase().includes(e.target.value.toLowerCase())) {
        return name;
      } else {
        return "";
      }
    })

    this.setState({
      filteredNames: filtered
    })
  };


  render() {
    return (
      <div className="Form-container">
        <label>
          <p>Claim Your Page</p>
          <input
            onChange={this.handleSearch}
            type="text"
            name="name"
            placeholder="Type Your Artist Name"
          />
          <ul className="result">
            {this.state.filteredNames.map(name => <li key={name}>{name}</li>)}
          </ul>
        </label>
      </div>
    );
  }
}

export default LandingSearch;
