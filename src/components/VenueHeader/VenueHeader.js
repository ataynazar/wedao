import React from 'react';
import Header from '../Header/Header';
import PagesNav from '../PagesNav/PagesNav';

const VenueHeader = () =>
  <Header>
    <PagesNav />
  </Header>

export default VenueHeader;