import React from 'react';
import VenueHeader from '../VenueHeader/VenueHeader';
import VenueBasicInfo from '../VenueBasicInfo/VenueBasicInfo';
import PerformedArtistsComponent from "../PerformedArtistsComponent/PerformedArtistsComponent";
import MarginedWrapper from '../MarginedWrapper/MarginedWrapper'
import ScheduleComponent from '../ScheduleComponent/ScheduleComponent'

const VenuePage = () => (
    <React.Fragment>
            <VenueHeader/>
        <VenueBasicInfo/>
        <MarginedWrapper>
            <PerformedArtistsComponent/>
        </MarginedWrapper>
        <MarginedWrapper flex={true}>
            <ScheduleComponent/>
            <ScheduleComponent/>
        </MarginedWrapper>
    </React.Fragment>
)

export default VenuePage;
