import React from 'react';
import './Steps.scss';
import social from './social.png';
import plan from './plan.png';
import ai from './ai.png';

const Steps = () => (
  <section className="landing-steps">
    <div className="step">
      <div className="step__content">
        <h2 className="step__title">
          Connect your social media <br /> and let us do the math for <br /> you
        </h2>
        <div className="step__text">
          <p>
            Where your fans are and what the box <br /> office could be for
            every city
          </p>
          <p>
            Embrace not only active fans, but <br /> potential audience
          </p>
          <p>Who will influence ticket sales the most</p>
        </div>
      </div>
      <div className="step__img">
        <img src={social} alt="social" />
      </div>
    </div>
    <div className="step">
      <div className="step__img">
        <img src={plan} alt="plan" />
      </div>
      <div className="step__content">
        <h2 className="step__title">Plan your tour</h2>
        <div className="step__text">
          <p>Match and book venues you like</p>
          <p>
            Get inquiries for shows from fans and<br/> venues
          </p>
          <p>Smart contract guarantees fair <br/>revenues split</p>
        </div>
      </div>
    </div>
    <div className="step">
      <div className="step__content">
        <h2 className="step__title">
          Let our AI engine execute<br /> and let us do the math for <br /> you
        </h2>
        <div className="step__text">
          <p>
            Where your fans are and what the box <br /> office could be for
            every city
          </p>
          <p>
            Embrace not only active fans, but <br /> potential audience
          </p>
          <p>Who will influence ticket sales the most</p>
        </div>
      </div>
      <div className="step__img">
        <img src={ai} alt="ai" />
        {/* <div className="picture" style={{backgroundImage: {ai}}}></div> */}
      </div>
    </div>
  </section>
);

export default Steps;