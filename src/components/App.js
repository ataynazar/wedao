import React, { Component } from 'react';
import VenuePage from './Venue-Page/VenuePage';

export default class App extends Component {  
  render() {
    return (
      <div className="App">
        <VenuePage />
      </div>
    )
  }
}