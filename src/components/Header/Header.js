import React from 'react';
import logo from "./Logo.svg";
import './Header.scss';
import '../Authorization/Authorization';
import Authorization from '../Authorization/Authorization';

const Header = ({ children }) => (
  <header className="main-header">
    <img src={logo} alt="logo" />
    {children}
    <Authorization />
  </header>
);

export default Header;