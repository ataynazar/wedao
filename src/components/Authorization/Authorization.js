import React from 'react';
import './Authorization.scss';

const Authorization = () =>
  <div className="authorization">
    <a href="#" className="sign-in">Sign In</a>
    <a href="#" className="register">Register</a>
  </div>

export default Authorization;