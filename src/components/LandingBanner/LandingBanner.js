import React from 'react';
import './LandingBanner.scss'
import backgroundImage from './ArtistLandingBanner.jpg';

const LandingBanner = () => (
  <div
    style={{ backgroundImage: `url(${backgroundImage})` }}
    className="landing-banner">
    <h1 className="landing-banner__title">
      Community-Driven Platform <br/> for Music Event Promotion
    </h1>
  </div>
);

export default LandingBanner;