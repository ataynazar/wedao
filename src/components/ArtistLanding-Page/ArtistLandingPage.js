import React, { Component } from "react";
import LandingHeader from "../LandingHeader/LandingHeader";
import LandingBanner from "../LandingBanner/LandingBanner";
import Steps from "../LandingSteps/Steps";
import Superpowers from "../Superpowers/Superpowers";
import LandingSearch from "../LandingSearch/LandingSearch";

const ArtistLandingPage = () => 
  (
    <React.Fragment>
      <LandingHeader />
      <LandingBanner />
      <Superpowers />
      <LandingSearch />
      <Steps />
    </React.Fragment>
  );

export default ArtistLandingPage;