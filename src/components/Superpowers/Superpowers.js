import React from 'react';
import './Superpowers.scss';
import brain from './brain.svg';
import target from "./target.svg";
import gift from "./gift.svg";

const Superpowers = () => (
  <div className="Superpowers">
    <h2 className="Superpowers__title">WeDAO gives artists superpowers</h2>
    <div className="Superpowers__list">
      <div className="Superpowers__item">
        <img src={brain} alt="brain icon" />
        <p>
          With artificial intelligence, we know exactly who your fans are & what
          they love.
        </p>
      </div>
      <div className="Superpowers__item">
        <img src={target} alt="target icon" />
        <p>
          We know how many fans you have and in which cities you can find them.
        </p>
      </div>
      <div className="Superpowers__item">
        <img src={gift} alt="gift icon" />
        <p>We do everything else. Free. No payments required</p>
      </div>
    </div>
  </div>
);

export default Superpowers;