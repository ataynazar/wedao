import React from 'react';
import './ScheduleComponent.scss'

const shows = [
    {
        month: "Mar",
        day: 18,
        artist: "Janet Jackson",
        daoRating: 75,
        daoRatingPercent: 13
    },
    {
        month: "Mar",
        day: 19,
        artist: "Depeche Mode",
        daoRating: 75,
        daoRatingPercent: 13
    },
    {
        month: "Mar",
        day: 21,
        artist: "Gipsy Kings",
        daoRating: 75,
        daoRatingPercent: 13
    },
]

const ScheduleComponent = () => (
    <div className="schedule-component-container">
        <div className="title-container">
            <h1 className="title">Upcoming Shows</h1>
            <div className="date-picker">MARCH 2018</div>
        </div>
        <table>
            <tr>
                <th>Date</th>
                <th>Artist</th>
                <th>Dao Rating</th>
            </tr>
            {shows.map(show =>
                (
                    <tr>
                        <td className="date">{show.month}<br/><span className="number">{show.day}</span></td>
                        <td>{show.artist}</td>
                        <td>{show.daoRating} <span className="percent">{`(+${show.daoRatingPercent}%)`}</span></td>
                    </tr>
                )
            )}

        </table>
        <a href="#" className="view-all">View all</a>
    </div>
);

export default ScheduleComponent;
