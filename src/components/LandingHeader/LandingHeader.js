import React, { Component } from "react";
import Header from "../Header/Header";
import LandingNav from "../LandingNav/LandingNav";
import Authorization from "../Authorization/Authorization";

export default class LandingHeader extends Component {
  render() {
    return (
      <Header>
        <LandingNav />
      </Header>
    );
  }
}
