import React from 'react';
import './LandingNav.scss';

const LandingNav = () =>
  <nav className="landing-nav">
    <a className="landing-nav__item" href="#">Artists</a>
    <a className="landing-nav__item" href="#">Venues</a>
    <a className="landing-nav__item" href="#">Influencers</a>
  </nav>

export default LandingNav;