import React from 'react';
import './PagesNav.scss';

const PagesNav = () =>
  <nav className="pages-nav">
    <a href="#" className="pages-nav__item">Trending</a>
    <a href="#" className="pages-nav__item">Discover</a>
  </nav>

export default PagesNav;