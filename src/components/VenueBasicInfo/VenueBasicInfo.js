import React from 'react';
import './VenueBasicInfo.scss';
import venueImage from './venue-image.png'
import fb from './fb.svg';
import instagram from './instagram.svg';
import tumbler from './tumbler.svg';
import twitter from './twitter.svg';
import youtube from './youtube.svg';

const VenueBasiInfo = () => {
  return (
    <div className="Venue-info-wrapper">
      <div
        className="Venue-image" 
        style={{ backgroundImage: `url(${venueImage})` }}>
      </div>
      <div className="Venue-info">
        <div className="info-text">
          <h2 className="label">Venue <a href="#" className="cta">Claim this venue</a></h2>
          <h1 className="Venue-title">Hollywood Bowl</h1>
          <table className="table">
            <tr>
              <th>Capacity</th>
              <th>Type</th>
              <th>Style</th>
              <th>Address</th>
            </tr>
            <tr>
              <td>17,500</td>
              <td>Outdoor</td>
              <td>Fancy</td>
              <td>
                2301 N Highland Ave Los Angeles, CA 90068
                <span className="address-links">
                  <a href="#" className="cta">Directions</a>
                  <a href="#" className="cta">Map</a>
                </span>
              </td>
            </tr>
          </table>
        </div>
        <ul className="Venue-socials">
          <li className="Venue-socials__item"><a href="#"><img src={fb} alt=""/></a></li>
          <li className="Venue-socials__item"><a href="#"><img src={twitter} alt=""/></a></li>
          <li className="Venue-socials__item"><a href="#"><img src={instagram} alt=""/></a></li>
          <li className="Venue-socials__item"><a href="#"><img src={tumbler} alt=""/></a></li>
          <li className="Venue-socials__item"><a href="#"><img src={youtube} alt=""/></a></li>
        </ul>
      </div>
    </div>
  )
}

export default VenueBasiInfo;